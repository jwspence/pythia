#!/bin/sh
if [ ! $?LD_LIBRARY_PATH ]; then
  export LD_LIBRARY_PATH=/afs/cern.ch/user/j/jwspence/WORK/FASER/pythia/HepMC-2.06.09/installed/lib
fi
if [ $?LD_LIBRARY_PATH ]; then
  export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/afs/cern.ch/user/j/jwspence/WORK/FASER/pythia/HepMC-2.06.09/installed/lib
fi
export PYTHIA8DATA=${PYTHIA8_HOME}/xmldoc
