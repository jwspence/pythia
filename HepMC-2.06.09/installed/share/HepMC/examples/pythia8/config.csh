#!/bin/csh
if( ! $?LD_LIBRARY_PATH ) then
  setenv LD_LIBRARY_PATH /afs/cern.ch/user/j/jwspence/WORK/FASER/pythia/HepMC-2.06.09/installed/lib
else
  setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:/afs/cern.ch/user/j/jwspence/WORK/FASER/pythia/HepMC-2.06.09/installed/lib
endif
setenv PYTHIA8DATA ${PYTHIA8_HOME}/xmldoc
