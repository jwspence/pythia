#!/bin/bash
cd HepMC-2.06.09
./configure --prefix=${PWD}/installed --with-momentum=GEV --with-length=MM
make ; make install
cd ..
cd LHAPDF-6.2.3
./configure --prefix=${PWD}/installed/
make ; make install
cd ../pythia8244/
./configure --with-hepmc2=${PWD}/../HepMC-2.06.09/installed --with-lhapdf6=${PWD}/../LHAPDF-6.2.3/installed
make ; make install
#cd examples
#make main42
#cp ~/public/neutrino-tungsten.cmnd .
#./main42 neutrino-tungsten.cmnd out42

